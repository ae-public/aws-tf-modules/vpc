resource "aws_vpc" "this" {
  cidr_block           = var.vpc_netblock
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "vpc"]),
  ))
}

# Internet GW
resource "aws_internet_gateway" "this" {
  vpc_id   = aws_vpc.this.id

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "igw"]),
  ))
}

# route tables
## See routes.tf

# Subnets
## See subnets.tf

# NAT
## See nat.tf
