locals {
  tags = "${map(
    "Environment", "${var.org_name}:${var.ou_name}:${var.acc_name}",
    "RequestedBy", "${var.org_name}-bm"
  )}"
}
