resource "aws_eip" "this" {
  vpc      = true

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "nat-eip"]),
  ))
}

resource "aws_nat_gateway" "this" {
  allocation_id = aws_eip.this.id
  subnet_id     = aws_subnet.public.*.id[0]
  depends_on    = [aws_internet_gateway]

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "nat-gw"]),
  ))
}

resource "aws_route" "nat-gw" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.this.id
}
