# route tables
resource "aws_route_table" "public" {
  vpc_id   = aws_vpc.this.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.this.id
  }

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "rt-pub"])
  ))
}

resource "aws_route_table" "private" {
  vpc_id   = aws_vpc.this.id

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "rt-prv"]),
  ))
}

# Isolated Subnets will remain in default route table.

# Routing
resource "aws_route_table_association" "pub" {
  count          = local.num_of_azs
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}
resource "aws_route_table_association" "prv" {
  count          = local.num_of_azs
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = aws_route_table.private.id
}
