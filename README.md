This module is intended to serve as a basic AWS VPC constructor whilst waiting for TLZ to be published.

Please note that this module is NOT intended to be used for environments requiring full-HA as,
for instance, it does not include redundant outbound NAT gateways.

To access this module from within terraform, please see Ref:1 below.

References:
1. https://www.terraform.io/docs/modules/sources.html
