variable "org_name" {
  type    = string
  default = "BoxChamp"
}

variable "ou_name" {
  description = "Name of OU"
  type        = string
}

variable "acc_name" {
  description = "Account Name"
  type        = string
}

variable "aws_region" {
  type    = string
  default = "us-east-2"
}

variable "vpc_netblock" {
  type    = string
  default = "10.0.0.0/16"
}

variable "org_account_id" {
  type    = string
  default = "440954914617"
}

variable "org_admin_role" {
  type    = string
  default = "OrgAdminRole"
}

variable "tags" {
  description = "Map of tags to add to all resources"
  type        = map(string)
  default     = {}
}