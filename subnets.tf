# Subnets
resource "aws_subnet" "public" {
  count                   = local.num_of_azs
  vpc_id                  = aws_vpc.this.id
  cidr_block              = cidrsubnet(var.vpc_netblock, 8, count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = "true"

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "pub", count.index]),
  ))
}

resource "aws_subnet" "private" {
  count                   = local.num_of_azs
  vpc_id                  = aws_vpc.this.id
  cidr_block              = cidrsubnet(var.vpc_netblock, 8, local.num_of_azs + count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = "false"

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "prv", count.index]),
  ))
}

# These subnets are intended for systems not requiring internet access (e.g. RDS)
resource "aws_subnet" "isolated" {
  count                   = local.num_of_azs
  vpc_id                  = aws_vpc.this.id
  cidr_block              = cidrsubnet(var.vpc_netblock, 8, 2 * local.num_of_azs + count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = "false"

  tags = merge(
    local.tags,
    var.tags,
    map(
      "Name", join("-", [var.org_name, "no-nat", count.index]),
  ))
}

locals {
  num_of_azs = length(data.aws_availability_zones.available.names)
}

data "aws_availability_zones" "available" {
  state    = "available"
}
