output "public_subnets" {
  value = aws_subnet.public.*.id
}

output "private_subnets" {
  value = aws_subnet.private.*.id
}

output "isolated_subnets" {
  value = aws_subnet.isolated.*.id
}

output "aws_ig" {
  value = aws_internet_gateway.this.id
}

output "vpc_id" {
  value = aws_vpc.this.id
}

output "az_ids" {
  value = data.aws_availability_zones.available.*.id
}

output "az_names" {
  value = data.aws_availability_zones.available.*.names[0]
}

output "pub_routing_table" {
  value = aws_route_table.public.id
}

output "prv_routing_table" {
  value = aws_route_table.private.id
}
